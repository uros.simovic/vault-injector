# vault-k8s-webhook-mutator

    ```
    | What          | Where                                                           |
    | ------------- | --------------------------------------------------------------- |
    | Documentation | [On kiwi.wiki](https://your-team.kiwi.wiki/this-thing)          |
    | Discussion    | [On Slack](https://skypicker.slack.com/messages/plz-your-team/) |
    | Maintainer    | [Mr. Picky](https://gitlab.skypicke.com/picky/)                 |
    ```

## Initial setup
This repo was created from a cookiecutter template.
Here are some instructions from the creators of that template:



## Usage

Project layout is based on [Standard Go Project Layout](https://github.com/golang-standards/project-layout), which is a community-driven effort to find and collect best practices when it comes to structuring Go code.

## Testing
```bash
make test
```

To run a coverage report:

```bash
make coverage
```

## Building
```bash
make build
```

This will produce a binary in `bin/` folder.

## Linting
```bash
make lint
```

## Additional services
```bash
make help
```

