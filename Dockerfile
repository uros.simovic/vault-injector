FROM alpine
LABEL maintainer="Uroš Simović <uros.simovic@kiwi.com>"

RUN apk add --update ca-certificates && rm -rf /var/cache/apk/*

ARG cmd
ENV cmd=$cmd

COPY bin/$cmd /bin/app
ENTRYPOINT ["/bin/app"]
