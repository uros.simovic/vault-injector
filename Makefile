.PHONY: build build-static clean test help coverage package tag

BIN_NAME:=vault-k8s-webhook-mutator
GO_PACKAGE_NAME:=gitlab.skypicker.com/platform/migration-and-infrastructure-squad/vault_k8s_webhook_mutator

VERSION?=0.0.0

TMP:=/tmp/$(BIN_NAME)
MAKE_TMP:=$(shell mkdir -p $(TMP))
CMDS=$(shell ls cmd)

COVERAGE_FILE:=$(TMP)/test-coverage.txt
MEMORY_BENCHMARK:=$(TMP)/mem.out
CPU_BENCHMARK:=$(TMP)/cpu.out
TRACE_FILE:=$(TMP)/race.out
SERVER_PID:=$(TMP)/server.pid

CMD=""
export CMD

default: build

#? build: compile project
build:
ifeq ($(CMD), "")
	@echo "Building commands..."
	@for CMD1 in $(CMDS); do \
		echo "Building $$CMD1"; \
		go build -o bin/$$CMD1 -v ./cmd/$$CMD1; \
	done
else
	@echo "Building command $(CMD)"
	go build -o bin/$(CMD) -v ./cmd/$(CMD);
endif


#? clean: clean the directory tree
clean:
	@test ! -e bin/${BIN_NAME} || rm bin/${BIN_NAME}

#? test: run tests
test:
	go test $(shell go list ./... | grep -v /.cache/ ) -v -coverprofile ${COVERAGE_FILE}

#? test-race-detector: run tests with race detector enabled
test-race-detector:
	go test $(shell go list ./... | grep -v /.cache/ ) -race

#? coverage: run tests with coverage report
coverage: test
	go tool cover -func=${COVERAGE_FILE}

#? godoc: run a local GoDoc server
godoc:
	@echo Starting local GoDoc server on port 8888
	@echo Open http://localhost:8888/pkg/$(GO_PACKAGE_NAME) in your browser
	@# This is a temporary fix because GoDoc has issues with Go modules,
	@# so the solution at the moment is to fake GOROOT and GOPATH
	@mkdir -p /tmp/tmpgoroot/doc
	@rm -rf /tmp/tmpgopath/src/$(GO_PACKAGE_NAME)
	@mkdir -p /tmp/tmpgopath/src/$(GO_PACKAGE_NAME)
	@tar -c --exclude='.git' --exclude='tmp' . | tar -x -C /tmp/tmpgopath/src/$(GO_PACKAGE_NAME)
	@GOROOT=/tmp/tmpgoroot/ GOPATH=/tmp/tmpgopath/ godoc -http="localhost:8888"

#? lint: run a meta linter
lint:
	docker run --rm -v $(shell pwd):/app --workdir=/app golangci/golangci-lint golangci-lint run

#? help: display help
help: Makefile
	@printf "Available make targets:\n\n"
	@sed -n 's/^#?//p' $< | column -t -s ':' |  sed -e 's/^/ /'

