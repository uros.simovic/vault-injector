package main

import (
	"flag"
	"gitlab.skypicker.com/platform/migration-and-infrastructure-squad/vault-k8s-webhook-mutator/pkg/csr"
	"log"
	"net"
)

var (
	serviceName = flag.String("service-name", "", "Service name")
	namespace   = flag.String("namespace", "", "Kubernetes namespace")
	podIP       = flag.String("pod-ip", "", "Pod IP address")
	kubeconfig  = flag.String("kubeconfig", "", "Kubernetes config path")
	certsDir    = flag.String("certs-dir", "certs", "directory to store .cert and .key file")
)

func main() {
	flag.Parse()

	if len(*namespace) == 0 {
		log.Fatal("--namespace is required")
	}
	if len(*serviceName) == 0 {
		log.Fatal("--service-name is required")
	}
	ip := net.ParseIP(*podIP)
	if ip.To4() == nil {
		log.Fatal("invalid --pod-ip address")
	}

	c := csr.NewCSR(csr.NewDefaultConfig(*serviceName, *namespace, *kubeconfig, ip))

	err := c.Send()
	if err != nil {
		log.Fatal(err)
	}

	if len(*certsDir) > 0 {
		err = c.WriteFiles(*certsDir)
		if err != nil {
			log.Fatal(err)
		}
	}
}
