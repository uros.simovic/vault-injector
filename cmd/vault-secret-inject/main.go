package main

import (
	"gitlab.skypicker.com/platform/migration-and-infrastructure-squad/vault-k8s-webhook-mutator/pkg/injector"
	"log"
)

func main() {
	err := injector.InjectSecrets()
	if err != nil {
		log.Fatal(err)
	}
}
