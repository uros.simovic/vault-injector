package main

import (
	"flag"
	"gitlab.skypicker.com/platform/migration-and-infrastructure-squad/vault-k8s-webhook-mutator/pkg/csr"
	"gitlab.skypicker.com/platform/migration-and-infrastructure-squad/vault-k8s-webhook-mutator/pkg/mutator"
	"log"
	"net"
)

var (
	serviceName = flag.String("service-name", "", "Service name")
	namespace   = flag.String("namespace", "", "Kubernetes namespace")
	podIP       = flag.String("pod-ip", "", "Pod IP address")
	kubeconfig  = flag.String("kubeconfig", "", "Kubernetes config path")
)

func main() {
	flag.Parse()

	if len(*namespace) == 0 {
		log.Fatal("--namespace is required")
	}
	if len(*serviceName) == 0 {
		log.Fatal("--service-name is required")
	}
	ip := net.ParseIP(*podIP)
	if ip.To4() == nil {
		log.Fatal("invalid --pod-ip address")
	}

	// CSR
	c := csr.NewCSR(csr.NewDefaultConfig(*serviceName, *namespace, *kubeconfig, ip))

	err := c.Send()
	if err != nil {
		log.Fatal(err)
	}

	cert, err := c.GetX509KeyPair()
	if err != nil {
		log.Fatal(err)
	}

	// Start mutating webhook server
	err = mutator.StartServer(cert)
	if err != nil {
		log.Fatal(err)
	}
}
