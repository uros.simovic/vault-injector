package injector

import (
	"encoding/json"
	"fmt"
	"github.com/pkg/errors"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"

	vaultapi "github.com/hashicorp/vault/api"
)

const (
	EnvKeyVaultAddr    = "VAULT_ADDR"
	EnvKeyVaultRole    = "VAULT_ROLE"
	EnvKeyVaultPath    = "VAULT_PATH"
	EnvKeyVaultSecrets = "VAULT_SECRETS"

	EnvSecretPrefix = "vault:"

	DefaultJwtSecretPath 	  = "/var/run/secrets/kubernetes.io/serviceaccount/token"
	DefaultSecretesVolumePath = "/home/vault/secrets"
)

var (
	jwtSecretPath		= ""
	secretesVolumePath 	= ""
)

type Client struct {
	cfg 	Config
	secrets []*Secret
	vault  	*vaultapi.Client
}

type Config struct {
	addr string
	role string
	path string
	jwt  string
}

type Secret struct {
	Name  string
	Path  string
	Key   string
	Idx   int
	value interface{}
}

func (s *Secret) writeToFile() error {

	// Write secret
	secretPath := filepath.Join(GetSecretsVolumeMountPath(s.Idx), s.Name)
	if err := ioutil.WriteFile(secretPath, []byte(s.value.(string)), 0640); err != nil {
		return errors.Wrapf(err, "error writing secret %s", secretPath)
	}

	log.Printf("Secret written to '%s'", secretPath)

	return nil
}

func (c *Client) parseEnvSecrets() error {

	j := os.Getenv(EnvKeyVaultSecrets)
	if j == "" {
		return errors.Errorf("Couldn't find env %s", EnvKeyVaultSecrets)
	}

	err := json.Unmarshal([]byte(j), &c.secrets)
	if err != nil {
		return errors.Errorf("Error underselling %s value", EnvKeyVaultSecrets)
	}

	return nil
}

func (c *Client) login() error {

	log.Println("Logging in with jwt")
	vaultClient, err := vaultapi.NewClient(vaultapi.DefaultConfig())
	if err != nil {
		return errors.Wrap(err, "error creating vault client")
	}

	err = vaultClient.SetAddress(c.cfg.addr)
	if err != nil {
		return errors.Wrap(err, "error setting vault address")
	}

	vaultResp, err := vaultClient.Logical().Write(
		c.cfg.path + "/login",
		map[string]interface{}{
			"role": c.cfg.role,
			"jwt":  c.cfg.jwt,
		})

	if err != nil {
		return errors.Wrap(err, "error while logging into vault with jwt")
	}

	log.Println("Logged in with jwt")
	log.Println("Getting token")

	token, err := vaultResp.TokenID()
	if err != nil {
		return errors.Wrap(err, "error getting token after login")
	}
	if token == "" {
		return errors.New("got the empty token")
	}

	vaultClient.SetToken(token)

	c.vault = vaultClient

	log.Println("Got the token")

	return nil
}

func (c *Client) readSecrets() error {

	log.Println("Reading secrets from vault")

	for _, s := range c.secrets {
		vaultResp, err := c.vault.Logical().Read(s.Path)
		if err != nil {
			return errors.Wrapf(err, "error reading secret '%s'", s.Path)
		}

		value, ok := vaultResp.Data[s.Key]
		if !ok {
			return errors.Errorf( "'%s' key not found in secret '%s'", s.Key, s.Path)
		}

		log.Printf("Got the secret '%s.%s'", s.Path, s.Key)

		s.value = value
	}

	return nil
}

func (c *Client) writeSecretsToFiles() error {
	log.Println("Writing secrets to files")

	for _, s := range c.secrets {
		err := s.writeToFile()
		if err != nil {
			return err
		}
	}

	return nil
}

func NewSecretFromEnv(key, value string, idx int) *Secret {
	// not vault secret env
	if !strings.HasPrefix(value, EnvSecretPrefix) {
		return nil
	}

	v := strings.TrimPrefix(value, EnvSecretPrefix)
	vv := strings.Split(v, "#")

	if len(vv) != 2 {
		log.Printf("env secret should be in format %s/path/to/secrets#key (%s)", EnvSecretPrefix, value)
		return nil
	}

	return &Secret{
		Name: key,
		Path: vv[0],
		Key:  vv[1],
		Idx:  idx,
	}
}

//func parseEnvSecret(s string) (*Secret, error) {
//
//	sn := strings.Split(s, "=")
//	if len(sn) != 2 {
//		// empty env
//		return nil, nil
//	}
//	name := sn[0]
//	s = sn[1]
//
//	if strings.HasPrefix(s, EnvSecretPrefix) {
//		s := strings.TrimPrefix(s, EnvSecretPrefix)
//
//		ss := strings.Split(s, "#")
//		if len(ss) == 2 {
//			sss := strings.Split(ss[1], "|")
//			if len(sss) == 2 {
//				idx, err := strconv.Atoi(sss[1])
//				if err != nil {
//					return nil, errors.Wrap(err, "error parsing env secret idx")
//				}
//
//				return &Secret{
//					Name: name,
//					Path: ss[0],
//					Key:  sss[0],
//					Idx:  idx,
//				}, nil
//			}
//		}
//
//		return nil, errors.Errorf("env secret should be in format %s/path/to/secrets#key|volumeMountIdx", EnvSecretPrefix)
//	}
//
//	// not our vault secret
//	return nil, nil
//}

func newConfigFromEnv() (*Config, error) {

	log.Println("Creating new config from ENV")

	c := Config{
		addr: os.Getenv(EnvKeyVaultAddr),
		role: os.Getenv(EnvKeyVaultRole),
		path: os.Getenv(EnvKeyVaultPath),
	}

	if c.addr == "" {
		return nil, errors.Errorf("error creating config: first set env %s", EnvKeyVaultAddr)
	}
	log.Printf("Env %s=%s", EnvKeyVaultAddr, c.addr)

	if c.role == "" {
		return nil, errors.Errorf("error creating config: first set env %s", EnvKeyVaultRole)
	}
	log.Printf("Env %s=%s", EnvKeyVaultRole, c.role)

	if c.path == "" {
		return nil, errors.Errorf("error creating config: first set env %s", EnvKeyVaultPath)
	}
	log.Printf("Env %s=%s", EnvKeyVaultPath, c.path)

	jsp := jwtSecretPath
	if jsp == "" {
		jsp = DefaultJwtSecretPath
	}

	log.Printf("Reading jwt from '%s'", jsp)
	jwt, err := ioutil.ReadFile(jsp)
	if err != nil {
		return nil, errors.Wrap(err, "error reading jwt secret file")
	}

	if secretesVolumePath == "" {
		secretesVolumePath = DefaultSecretesVolumePath
	}


	c.jwt = string(jwt)

	log.Printf("Got the jwt!")

	return &c, nil
}

func InjectSecrets() error {
	log.Println("Starting vault injector...")

	cfg, err := newConfigFromEnv()
	if err != nil {
		return err
	}

	c := Client{
		cfg: *cfg,
	}

	err = c.parseEnvSecrets()
	if err != nil {
		return err
	}

	err = c.login()
	if err != nil {
		return err
	}

	err = c.readSecrets()
	if err != nil {
		return err
	}

	err = c.writeSecretsToFiles()
	if err != nil {
		return err
	}

	return nil
}

func SetJwtSecretPath(path string) {
	jwtSecretPath = path
}

func SetSecretsVolumPath(path string) {
	secretesVolumePath = path
}

func GetSecretsVolumeMountPath(idx int) string {
	svp := secretesVolumePath
	if svp == "" {
		svp = DefaultSecretesVolumePath
	}

	return fmt.Sprintf("%s_%d", svp, idx)
}
