package injector

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"testing"
)

var (
	responses = []string{
		"test_responses/login.json",
		"test_responses/read_secret.json",
	}
	i = 0
)

func TestInjectSecrets(t *testing.T) {
	// mock vault
	fakeVault := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		toReturn, err := ioutil.ReadFile(responses[i])
		if err != nil {
			t.Fatal(err)
		}
		i++

		w.Header().Set("Content-Type", "application/json")
		fmt.Fprintln(w, string(toReturn))
	}))
	defer fakeVault.Close()

	// jwt token
	jwt, err := ioutil.TempFile(os.TempDir(), "test_token.jwt")
	if err != nil {
		t.Fatal("Cannot create test cube config file", err)
	}
	defer os.Remove(jwt.Name())

	if _, err = jwt.Write([]byte("test.token.data")); err != nil {
		t.Fatal("Failed to write test cube config file", err)
	}
	SetJwtSecretPath(jwt.Name())

	// secrets path
	tmpSecretsDir, err := ioutil.TempDir(os.TempDir(), "test_token")
	if err != nil {
		t.Fatal("Cannot create temp dir", err)
	}
	tmpSecretsDirSuffix := tmpSecretsDir + "_1"

	defer os.RemoveAll(tmpSecretsDir)
	defer os.RemoveAll(tmpSecretsDirSuffix)

	err = os.Mkdir(tmpSecretsDirSuffix, 0700)
	if err != nil && !os.IsExist(err) {
		t.Fatal(err)
	}

	SetSecretsVolumPath(tmpSecretsDir)

	// set env
	err = os.Setenv(EnvKeyVaultAddr, fakeVault.URL)
	if err != nil {
		t.Fatal(err)
	}
	err = os.Setenv(EnvKeyVaultRole, "test-role")
	if err != nil {
		t.Fatal(err)
	}
	err = os.Setenv(EnvKeyVaultPath, "auth/kubernetes/test/cluster-test")
	if err != nil {
		t.Fatal(err)
	}
	err = os.Setenv(EnvKeyVaultSecrets, `[{"Name": "TEST_SECRET", "Path": "/secret/test/path", "Key": "TEST_SECRET", "Idx": 1}]`)
	if err != nil {
		t.Fatal(err)
	}

	// test main
	err = InjectSecrets()
	if err != nil {
		t.Fatal(err)
	}

	// read secret value
	secretFile, err := ioutil.ReadFile(filepath.Join(tmpSecretsDirSuffix, "TEST_SECRET"))
	if err != nil {
		t.Fatal(err)
	}

	shouldValue := "TEST_VALUE"

	if string(secretFile) != shouldValue {
		t.Fatalf("TEST_SECRET value is %s, should be %s", string(secretFile), shouldValue)
	}
}
