package mutator

import (
	"bytes"
	"encoding/base64"
	"github.com/slok/kubewebhook/pkg/log"
	"github.com/tidwall/gjson"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestVaultMutatorWebHandler(t *testing.T) {

	whHandler, err := createHandler(&log.Std{})
	if err != nil {
		t.Fatal(err)
	}

	b, err := ioutil.ReadFile("test_json/request.json")
	if err != nil {
		t.Fatal(err)
	}

	req, err := http.NewRequest(http.MethodPost, "/pod", bytes.NewBuffer(b))
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	whHandler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Fatalf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	value := gjson.Get(rr.Body.String(), "response.patch")


	if value.String() == "" {
		t.Fatal("'response.patch' not exists in response json")
	}

	d, err := base64.StdEncoding.DecodeString(value.String())
	if err != nil {
		t.Fatalf("error base64 decoding 'response.patch': %v", err)
	}

	if string(d) == "[]" {
		t.Fatal("'response.patch' is empty")
	}
}
