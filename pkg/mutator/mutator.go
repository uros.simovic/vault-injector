package mutator

import (
	"context"
	"crypto/tls"
	"encoding/json"
	"fmt"
	whhttp "github.com/slok/kubewebhook/pkg/http"
	"github.com/slok/kubewebhook/pkg/log"
	"github.com/slok/kubewebhook/pkg/webhook/mutating"
	"gitlab.skypicker.com/platform/migration-and-infrastructure-squad/vault-k8s-webhook-mutator/pkg/injector"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"net/http"
	"strings"
)

const (
	AnnotationVaultAddr = "vault.skypicker.io/addr"
	AnnotationVaultRole = "vault.skypicker.io/role"
	AnnotationVaultPath = "vault.skypicker.io/path"

	//SecretesVolumeName = "vault-secretes"
)

type VaultMutator struct {
	vaultAddr string
	vaultRole string
	vaultPath string

	pod *corev1.Pod

	idx int

	volumes          []corev1.Volume
	initSecrets      []*injector.Secret
	initVolumeMounts []corev1.VolumeMount
}

type EnvVar struct {
	Original  corev1.EnvVar
	Container int
}

func envIsVaultSecret(env corev1.EnvVar) bool {
	return strings.HasPrefix(env.Value, injector.EnvSecretPrefix)
}

func (vm *VaultMutator) mutateContainer(container *corev1.Container) *corev1.Container {

	secretEnvs := []corev1.EnvVar{}

	for _, env := range container.Env {
		if envIsVaultSecret(env) {
			secretEnvs = append(secretEnvs, env)
		}
	}

	if len(secretEnvs) == 0 {
		return container
	}

	vm.idx++

	volume := corev1.Volume{
		Name: fmt.Sprintf("vault-secretes-%d", vm.idx),
		VolumeSource: corev1.VolumeSource{
			EmptyDir: &corev1.EmptyDirVolumeSource{
				Medium: corev1.StorageMediumMemory,
			},
		},
	}
	vm.volumes = append(vm.volumes, volume)

	vm.initVolumeMounts = append(vm.initVolumeMounts, corev1.VolumeMount{
		Name:      volume.Name,
		MountPath: injector.GetSecretsVolumeMountPath(vm.idx),
	})

	container.VolumeMounts = append(container.VolumeMounts, corev1.VolumeMount{
		Name:      volume.Name,
		MountPath: injector.DefaultSecretesVolumePath,
		ReadOnly:  true,
	})

	for _, env := range secretEnvs {
		s := injector.NewSecretFromEnv(env.Name, env.Value, vm.idx)
		if s != nil {
			vm.initSecrets = append(vm.initSecrets, s)
		}
	}

	return container
}

func (vm *VaultMutator) verifyVaultVariables() bool {
	if vm.vaultAddr == "" || vm.vaultRole == "" || vm.vaultPath == "" {
		return false
	}

	return true
}

func (vm *VaultMutator) prepareInitEnvs() ([]corev1.EnvVar, error) {

	s, err := json.Marshal(vm.initSecrets)
	if err != nil {
		return nil, err
	}

	return []corev1.EnvVar{
		{
			Name:  injector.EnvKeyVaultAddr,
			Value: vm.vaultAddr,
		},
		{
			Name:  injector.EnvKeyVaultRole,
			Value: vm.vaultRole,
		},
		{
			Name:  injector.EnvKeyVaultPath,
			Value: vm.vaultPath,
		},
		{
			Name:  injector.EnvKeyVaultSecrets,
			Value: string(s),
		},
	}, nil
}

func (vm *VaultMutator) mutate() error {
	// add private registry secret
	vm.pod.Spec.ImagePullSecrets = append(vm.pod.Spec.ImagePullSecrets, corev1.LocalObjectReference{Name: "gitlab"})

	// mutate init containers first
	for i, c := range vm.pod.Spec.InitContainers {
		vm.pod.Spec.InitContainers[i] = *vm.mutateContainer(&c)
	}

	// then normal ones
	for i, c := range vm.pod.Spec.Containers {
		vm.pod.Spec.Containers[i] = *vm.mutateContainer(&c)
	}

	//append secret volumes to pod
	vm.pod.Spec.Volumes = append(vm.pod.Spec.Volumes, vm.volumes...)

	//init secretes volume mounts
	initVolumeMounts := vm.initVolumeMounts

	// get SA secret volume mount and add it to init Container
	// it seems mounting service account happens before this
	serviceAccountVM := findServiceAccountMountFromContainers(vm.pod.Spec.Containers)
	if serviceAccountVM != nil {
		initVolumeMounts = append(initVolumeMounts, *serviceAccountVM)
	}

	envs, err := vm.prepareInitEnvs()
	if err != nil {
		return err
	}

	initVaultContainer := corev1.Container{
		Name:            "vault-secrets-injector",
		Image:           "registry.skypicker.com:5005/platform/migration-and-infrastructure-squad/vault-k8s-webhook-mutator/vault-secret-inject:v1",
		ImagePullPolicy: corev1.PullAlways,
		Command:         []string{},
		VolumeMounts:    initVolumeMounts,
		Env:             envs,
	}

	// prepend vault inject init Container
	vm.pod.Spec.InitContainers = append([]corev1.Container{initVaultContainer}, vm.pod.Spec.InitContainers...)

	return nil
}

func findServiceAccountMountFromContainers(cs []corev1.Container) *corev1.VolumeMount {
	for _, c := range cs {
		for _, vm := range c.VolumeMounts {
			if vm.MountPath == "/var/run/secrets/kubernetes.io/serviceaccount" {
				return &vm
			}
		}
	}

	return nil
}

func podMutator(_ context.Context, obj metav1.Object) (bool, error) {

	pod, ok := obj.(*corev1.Pod)
	if !ok {
		// If not a pod just continue the mutation chain(if there is one) and don't do nothing.
		return false, nil
	}

	vm := NewVaultMutator(pod)
	if vm != nil {
		err := vm.mutate()
		if err != nil {
			return false, err
		}
	}

	return false, nil
}

func NewVaultMutator(pod *corev1.Pod) *VaultMutator {

	vm := VaultMutator{}

	vm.vaultAddr = pod.Annotations[AnnotationVaultAddr]
	vm.vaultRole = pod.Annotations[AnnotationVaultRole]
	vm.vaultPath = pod.Annotations[AnnotationVaultPath]

	if !vm.verifyVaultVariables() {
		return nil
	}

	vaultSecrets := 0

	for _, initContainer := range pod.Spec.InitContainers {
		for _, env := range initContainer.Env {
			if envIsVaultSecret(env) {
				vaultSecrets++
			}
		}
	}

	for _, container := range pod.Spec.Containers {
		for _, env := range container.Env {
			if envIsVaultSecret(env) {
				vaultSecrets++
			}
		}
	}

	if vaultSecrets > 0 {
		vm.pod = pod
		return &vm
	}

	return nil
}

func StartServer(certificate tls.Certificate) error {
	logger := &log.Std{Debug: true}

	h, err := createHandler(logger)
	if err != nil {
		return err
	}

	mux := http.NewServeMux()
	mux.Handle("/pods", h)

	logger.Infof("Start server on port 443...")

	err = httpsServer(":443", certificate, mux)

	if err != nil {
		return fmt.Errorf("error creating server: %v", err)
	}

	return nil
}

func httpsServer(addr string, certificate tls.Certificate, handler http.Handler) error {
	server := &http.Server{
		Addr:    addr,
		Handler: handler,
		TLSConfig: &tls.Config{
			Certificates: []tls.Certificate{certificate},
		},
	}

	return server.ListenAndServeTLS("", "")
}

func createHandler(logger log.Logger) (http.Handler, error) {

	mt := mutating.MutatorFunc(podMutator)

	mcfg := mutating.WebhookConfig{
		Name: "vault-secrets",
		Obj:  &corev1.Pod{},
	}

	wh, err := mutating.NewWebhook(mcfg, mt, nil, nil, logger)
	if err != nil {
		return nil, fmt.Errorf("error creating webhook: %v", err)
	}

	whHandler, err := whhttp.HandlerFor(wh)
	if err != nil {
		return nil, fmt.Errorf("error creating webhook handler: %v", err)
	}

	return whHandler, nil
}
