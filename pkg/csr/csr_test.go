package csr

import (
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"testing"
)

var (
	responses = []string{
		"test_responses/delete.json",
		"test_responses/create.json",
		"test_responses/approve.json",
		"test_responses/approved.json",
		"test_responses/delete.json",
	}
	i = 0
)

func TestCSRSendAndWrite(t *testing.T) {
	cluster := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		toReturn, err := ioutil.ReadFile(responses[i])
		if err != nil {
			t.Fatal(err)
		}
		i++

		w.Header().Set("Content-Type", "application/json")
		fmt.Fprintln(w, string(toReturn))
	}))
	defer cluster.Close()

	// Test cube config
	u, err := url.Parse(cluster.URL)
	if err != nil {
		t.Fatal(err)
	}

	kubeCfg := `
apiVersion: v1
clusters:
- cluster:
    insecure-skip-tls-verify: true
    server: http://%s
  name: test-cluster
contexts:
- context:
    cluster: test-cluster
    namespace: default
    user: test-user
  name: test
current-context: test
kind: Config
preferences: {}
users:
- name: test-user
`
	kubeCfg = fmt.Sprintf(kubeCfg, u.Host)

	tmpFile, err := ioutil.TempFile(os.TempDir(), "test-kubeconfig")
	if err != nil {
		t.Fatal("Cannot create test cube config file", err)
	}
	defer os.Remove(tmpFile.Name())

	if _, err = tmpFile.Write([]byte(kubeCfg)); err != nil {
		t.Fatal("Failed to write test cube config file", err)
	}

	cfg := NewDefaultConfig("test-service", "test-ns", tmpFile.Name(), net.ParseIP("1.2.3.4"))
	cfg.prependCA = "" //disable prepand

	tmpCertsDir, err := ioutil.TempDir(os.TempDir(), "test-cert")
	if err != nil {
		t.Fatal("Cannot create temp dir", err)
	}
	defer os.RemoveAll(tmpCertsDir)

	c := NewCSR(cfg)
	err = c.Send()
	if err != nil {
		t.Fatal(err)
	}

	err = c.WriteFiles(tmpCertsDir)
	if err != nil {
		t.Fatal(err)
	}
}
