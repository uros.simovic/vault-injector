package csr

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"fmt"
	"github.com/pkg/errors"
	"io/ioutil"
	certificates "k8s.io/api/certificates/v1beta1"
	types "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/client-go/kubernetes"
	_ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
	"k8s.io/client-go/tools/clientcmd"
	"log"
	"net"
	"os"
	"path/filepath"
	"strings"
	"time"
)

type CSR struct {
	name   string
	config Config
	key    []byte
	crt    []byte
}

type Config struct {
	keySize        int           //RSA key size in bits
	prependCA      string        //Prepend cluster's CA to certificate path
	serviceName    string        //Service name
	namespace      string        //Kubernetes namespace
	kubeconfig	   string        //Config file if running from outside of the cluster
	podIP          net.IP        //Pod IP address
	timeout        time.Duration //Timeout for auto approve csr
}

func NewDefaultConfig(serviceName, namespace, kubeconfig string, podIP net.IP) Config {
	cfg := Config{
		serviceName: serviceName,
		namespace:   namespace,
		podIP:       podIP,
		kubeconfig:  kubeconfig,

		timeout:   10 * time.Second,
		keySize:   2048,
		//prependCA: defaultCAPath,
		prependCA: "",
	}

	return cfg
}

func NewCSR(cfg Config) *CSR {
	return &CSR{
		name:   fmt.Sprintf("%s.%s.svc", cfg.serviceName, cfg.namespace),
		config: cfg,
	}
}

func (c *CSR) GetX509KeyPair() (tls.Certificate, error) {
	return tls.X509KeyPair(c.crt, c.key)
}

func (c *CSR) GeKeyPair() (key []byte, crt []byte) {
	return c.key, c.crt
}

func (c *CSR) Send() error {
	csrTemplate := c.createCSRTemplate()

	privateKey, privateKeyPEM, err := c.createKey()
	if err != nil {
		return err
	}

	csrPEM, err := c.createCSR(csrTemplate, privateKey)
	if err != nil {
		return err
	}

	crtPEM, err := c.signCSR(csrPEM)
	if err != nil {
		return err
	}

	if len(c.config.prependCA) > 0 {
		ca, err := ioutil.ReadFile(c.config.prependCA)
		if err != nil {
			return err
		}
		crtPEM = append(ca, crtPEM...)
	}

	c.key = privateKeyPEM
	c.crt = crtPEM

	return nil
}

func (c *CSR) createKey() (*rsa.PrivateKey, []byte, error) {
	log.Printf("Generating %d bits long private key\n", c.config.keySize)

	// Create KEY
	privateKey, err := rsa.GenerateKey(rand.Reader, c.config.keySize)
	if err != nil {
		return nil, nil, errors.Wrap(err, "error generating RSA key pair")
	}

	// Convert key to PEM format
	privateKeyPEM := pem.EncodeToMemory(
		&pem.Block{
			Type:  "RSA PRIVATE KEY",
			Bytes: x509.MarshalPKCS1PrivateKey(privateKey),
		},
	)

	if privateKeyPEM == nil {
		return nil, nil, errors.New("error converting key to PEM format")
	}

	return privateKey, privateKeyPEM, nil
}

func (c *CSR) createCSRTemplate() *x509.CertificateRequest {
	ipAddresses := []net.IP{net.ParseIP("127.0.0.1"), c.config.podIP}

	// DNS names
	dnsNames := []string{
		c.name,
		fmt.Sprintf("%s.%s", c.config.serviceName, c.config.namespace),
		fmt.Sprintf("%s.%s.svc", c.config.serviceName, c.config.namespace),
	}

	log.Printf("CSR DNS names: %s\n", strings.Join(dnsNames, ", "))

	csrTemplate := &x509.CertificateRequest{
		Subject: pkix.Name{
			Organization: []string{"kiwi.com"},
			CommonName:   c.config.serviceName,
		},
		//SignatureAlgorithm: x509.SHA256WithRSA,
		DNSNames:    dnsNames,
		IPAddresses: ipAddresses,
	}

	return csrTemplate
}

func (c *CSR) createCSR(template *x509.CertificateRequest, privateKey *rsa.PrivateKey) ([]byte, error) {
	log.Println("Creating CSR...")

	// Create CSR
	csrBytes, err := x509.CreateCertificateRequest(rand.Reader, template, privateKey)
	if err != nil {
		return nil, errors.Wrap(err, "error creating certificate request")
	}

	// Convert CSR to PEM.
	csrPEM := pem.EncodeToMemory(
		&pem.Block{
			Type:  "CERTIFICATE REQUEST",
			Bytes: csrBytes,
		},
	)

	if csrPEM == nil {
		return nil, errors.New("error converting CSR to PEM format")
	}

	return csrPEM, nil
}

func (c *CSR) signCSR(csrPEM []byte) ([]byte, error) {
	// Client init
	client, err := getKubernetesClient(c.config.kubeconfig)
	if err != nil {
		return nil, errors.Wrap(err, "Error creating k8s client")
	}

	// Delete if exists
	log.Println("Deleting previous csr if exists...")
	err = client.CertificatesV1beta1().CertificateSigningRequests().Delete(c.name, &types.DeleteOptions{})
	if err != nil {
		log.Println(err)
	} else {
		log.Printf("CSR '%s' deleted!\n", c.name)
	}

	// Send CSR to cluster
	log.Println("Sending CSR to cluster")

	keyUsages := []certificates.KeyUsage{
		certificates.UsageDigitalSignature,
		certificates.UsageKeyEncipherment,
		certificates.UsageClientAuth,
	}

	req := certificates.CertificateSigningRequest{
		TypeMeta:   types.TypeMeta{Kind: "CertificateSigningRequest"},
		ObjectMeta: types.ObjectMeta{Name: c.name},
		Spec: certificates.CertificateSigningRequestSpec{
			Request: csrPEM,
			Usages:  keyUsages,
		},
	}

	_, err = client.CertificatesV1beta1().CertificateSigningRequests().Create(&req)
	if err != nil {
		return nil, errors.Wrapf(err, "Error sending CSR '%s' to cluster", c.name)
	}

	log.Printf("CSR '%s' sent, waiting for approval\n", c.name)

	req.Status.Conditions = append(req.Status.Conditions, certificates.CertificateSigningRequestCondition{
		Type: certificates.CertificateApproved,
	})

	resp, err := client.CertificatesV1beta1().CertificateSigningRequests().UpdateApproval(&req)
	if err != nil {
		return nil, errors.Wrapf(err, "Auto Approve failed")
	}

	// Watch event waiting for approval
	log.Println("Creating watch event")
	timeout := int64(c.config.timeout.Seconds())

	watchReq := types.ListOptions{
		Watch:          true,
		TimeoutSeconds: &timeout,
		FieldSelector:  fields.OneTermEqualSelector("metadata.name", c.name).String(),
	}

	resultCh, err := client.CertificatesV1beta1().CertificateSigningRequests().Watch(watchReq)
	if err != nil {
		return nil, errors.Wrapf(err, "Error creating watch event for CSR '%s'", c.name)
	}

	log.Println("Watch event sent")

	watchCh := resultCh.ResultChan()

	for event := range watchCh {
		if event.Object.(*certificates.CertificateSigningRequest).UID != resp.UID {
			// Wrong object.
			log.Printf("received watch notification for object %v, but expected UID=%s\n", event.Object, resp.UID)
			continue
		}

		status := event.Object.(*certificates.CertificateSigningRequest).Status
		if len(status.Conditions) == 0 {
			continue
		}

		// So the latest one should be fine.
		cond := status.Conditions[len(status.Conditions)-1]
		if cond.Type != certificates.CertificateApproved {
			return nil, errors.Errorf("CSR not approved: %+v", status)
		}

		// This seems to happen: https://github.com/kubernetes/kubernetes/issues/47911
		if status.Certificate == nil {
			log.Printf("CSR approved, but no certificate in response. Waiting some more...\n")
			continue
		}

		log.Printf("CSR approved, got the signed certificate!\n")

		err = client.CertificatesV1beta1().CertificateSigningRequests().Delete(c.name, &types.DeleteOptions{})
		if err != nil {
			log.Println(err)
		} else {
			log.Printf("CSR '%s' deleted\n", c.name)
		}

		return status.Certificate, nil
	}

	return nil, errors.New("Error on the watch channel (timeout)")
}

func getKubernetesClient(kubeconfigPath string) (*kubernetes.Clientset, error) {
	// Config from flag or from cluster if empty
	config, err := clientcmd.BuildConfigFromFlags("", kubeconfigPath)
	if err != nil {
		return nil, errors.Wrap(err, "error building kubernetes config")
	}

	// Create the client.
	c, err := kubernetes.NewForConfig(config)
	if err != nil {
		return nil, errors.Wrap(err, "error creating kubernetes client")
	}

	return c, err
}


func (c *CSR) WriteFiles(dir string) error {
	if len(c.key) == 0 || len(c.crt) == 0 {
		return errors.New("nothing to write, first send csr")
	}

	// Make directory, but don't fail if exists
	if err := os.MkdirAll(dir, 0755); err != nil {
		return errors.Wrapf(err, "error creating directory '%s'", dir)
	}

	// Write key.
	keyPath := filepath.Join(dir, c.name+".key")
	if err := ioutil.WriteFile(keyPath, c.key, 0600); err != nil {
		return errors.Wrapf(err, "error writing private key file %s", keyPath)
	}
	log.Printf("Wrote key file: %s\n", keyPath)

	// Write certificate.
	certPath := filepath.Join(dir, c.name+".crt")
	if err := ioutil.WriteFile(certPath, c.crt, 0644); err != nil {
		return errors.Wrapf(err, "could not write certificate file %s", certPath)
	}
	log.Printf("Wrote certificate file: %s\n", certPath)

	return nil
}